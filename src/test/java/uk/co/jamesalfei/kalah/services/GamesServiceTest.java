package uk.co.jamesalfei.kalah.services;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ GamesServiceStartGameTest.class, GamesServicesMakeMoveTest.class })
public class GamesServiceTest {
	// Do nothing
}
