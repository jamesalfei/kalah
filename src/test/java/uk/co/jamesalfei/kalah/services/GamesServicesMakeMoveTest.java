package uk.co.jamesalfei.kalah.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import uk.co.jamesalfei.kalah.beans.Game;
import uk.co.jamesalfei.kalah.exceptions.GameNotFoundException;
import uk.co.jamesalfei.kalah.exceptions.GameNotYetStartedException;
import uk.co.jamesalfei.kalah.exceptions.GameOverException;
import uk.co.jamesalfei.kalah.exceptions.InGameException;
import uk.co.jamesalfei.kalah.exceptions.InvalidPitIdGivenException;
import uk.co.jamesalfei.kalah.exceptions.NoStonesInSelectedPitException;
import uk.co.jamesalfei.kalah.exceptions.PitIsHomePitException;
import uk.co.jamesalfei.kalah.exceptions.PitNotOwnedByPlayerException;

public class GamesServicesMakeMoveTest {

	GamesService service;
	URI uri;
	String gameId;

	private final static List<Integer> sampleGameMoves = new ArrayList<>();
	static {
		sampleGameMoves.addAll(
				Arrays.asList(5, 12, 0, 8, 1, 7, 3, 9, 2, 11, 3, 2, 12, 4, 0, 12, 11, 1, 12, 10, 5, 10, 0, 9, 2, 7, 1,
						9, 0, 10, 8, 3, 12, 2, 3, 10, 1, 8, 4, 7, 5, 11, 3, 12, 0, 10, 7, 2, 8, 4, 12, 11, 1, 12));
	}

	@Rule
	public ExpectedException ex = ExpectedException.none();

	@Before
	public void setUp() throws URISyntaxException {
		service = new GamesService();
		uri = new URI("test");

		// Initialise a new game with 2 players
		service.startNewGame(uri);
		Game game = service.startNewGame(uri);

		gameId = game.getId();
	}

	@Test
	public void testMakeMove() throws InGameException {
		service.makeMove(gameId, 4);
	}

	@Test
	public void testMakeMoveWithInvalidGameId() throws InGameException {
		ex.expect(GameNotFoundException.class);

		service.makeMove("123", 4);
	}

	@Test
	public void testMakeMoveWithInactiveGameId() throws InGameException {
		Game game = service.startNewGame(uri);

		ex.expect(GameNotYetStartedException.class);

		service.makeMove(game.getId(), 4);
	}

	@Test
	public void testMakeMoveWithPitWithNoStones() throws InGameException {
		ex.expect(NoStonesInSelectedPitException.class);

		service.makeMove(gameId, 4);
		service.makeMove(gameId, 7);
		service.makeMove(gameId, 4);
	}

	@Test
	public void testMakeMoveWithNegativePitId() throws InGameException {
		ex.expect(InvalidPitIdGivenException.class);
		ex.expectMessage("Given pit ID is less than 1. Please select a pit ID between 1 and 14");

		service.makeMove(gameId, -1);
	}

	@Test
	public void testMakeMoveWithInvalidPitId() throws InGameException {
		ex.expect(InvalidPitIdGivenException.class);
		ex.expectMessage(
				"Given pit ID exceeds the number of pits in this game. Please select a pit ID between 1 and 14");

		service.makeMove(gameId, 94);
	}

	@Test
	public void testMakeMoveWithUnplayablePitId() throws InGameException {
		ex.expect(PitIsHomePitException.class);

		service.makeMove(gameId, 6);
	}

	@Test
	public void testMakeMoveWithOpponentPitId() throws InGameException {
		ex.expect(PitNotOwnedByPlayerException.class);

		service.makeMove(gameId, 9);
	}

	@Test
	public void testMakeMoveGameOver() throws InGameException {
		ex.expect(GameOverException.class);

		for (int pitId : sampleGameMoves) {
			service.makeMove(gameId, pitId);
		}
	}

}
