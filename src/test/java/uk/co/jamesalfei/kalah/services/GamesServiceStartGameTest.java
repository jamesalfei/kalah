package uk.co.jamesalfei.kalah.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import uk.co.jamesalfei.kalah.beans.Game;

public class GamesServiceStartGameTest {

	GamesService service;
	URI uri;

	@Before
	public void setUp() throws URISyntaxException {
		service = new GamesService();
		uri = new URI("test");
	}

	@Test
	public void testStartNewGameNotNull() {
		Game result = service.startNewGame(uri);

		assertNotNull(result);
	}

	@Test
	public void testStartNewGameOnceHasPlayer() throws URISyntaxException {
		Game result = service.startNewGame(uri);

		assertEquals(1, result.getPlayers().size());
	}

	@Test
	public void testStartNewGameTwiceHasTwoPlayers() {
		Game initialResult = service.startNewGame(uri);
		Game secondResult = service.startNewGame(uri);

		assertEquals(initialResult.getId(), secondResult.getId());
		assertEquals(2, secondResult.getPlayers().size());
	}

	@Test
	public void testStartNewGameThreeTimesHasThreePlayers() {
		Game firstResult = service.startNewGame(uri);
		service.startNewGame(uri);
		Game thirdResult = service.startNewGame(uri);

		assertNotEquals(firstResult.getId(), thirdResult.getId());
		assertEquals(1, thirdResult.getPlayers().size());
	}

}
