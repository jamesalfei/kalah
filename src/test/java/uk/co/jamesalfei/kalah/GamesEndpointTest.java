package uk.co.jamesalfei.kalah;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import uk.co.jamesalfei.kalah.beans.Game;
import uk.co.jamesalfei.kalah.beans.HomePit;
import uk.co.jamesalfei.kalah.exceptions.GameNotYetStartedException;
import uk.co.jamesalfei.kalah.exceptions.GameOverException;
import uk.co.jamesalfei.kalah.services.GamesService;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GamesEndpointTest {

	@Mock
	private GamesService gamesService;

	@Mock
	private UriInfo uriInfo;

	@InjectMocks
	private GamesEndpoint gamesEndpoint;

	@Before
	public void setUp() throws Exception {

	    Game game = new Game("", 2, 6, 6);

		when(uriInfo.getAbsolutePath()).thenReturn(new URI("unitTest/path"));
		when(gamesService.startNewGame(any())).thenReturn(game);
        when(gamesService.makeMove(anyString(), anyInt())).thenReturn(game);
	}

	@Test
	public void testStartNewGame() throws Exception {
		Response resp = gamesEndpoint.startNewGame(uriInfo);

		assertEquals(201, resp.getStatus());
		assertNotNull(resp.getEntity());
	}

	@Test
	public void testPerformMove() throws Exception {
		Response result = gamesEndpoint.performMove("123", 2);

		assertEquals(200, result.getStatus());
		assertNotNull(result.getEntity());
	}

	@Test
	public void testPerformMoveThrowsGameOverException() throws Exception {
        when(gamesService.makeMove(anyString(), anyInt())).thenThrow(new GameOverException(new HomePit(2)));

        Response result = gamesEndpoint.performMove("123", 2);

        assertEquals(200, result.getStatus());
        assertTrue(result.getEntity() instanceof HomePit);
	}

	@Test
	public void testPerformMoveThrowsInGameException() throws Exception {
        when(gamesService.makeMove(anyString(), anyInt())).thenThrow(new GameNotYetStartedException());

        Response result = gamesEndpoint.performMove("123", 2);

        assertEquals(400, result.getStatus());
        assertEquals("There are not yet enough players in the game. Please wait for another player to join", result.getEntity().toString());
	}

}
