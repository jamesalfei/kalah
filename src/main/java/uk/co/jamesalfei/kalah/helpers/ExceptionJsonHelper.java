package uk.co.jamesalfei.kalah.helpers;

/**
 * Simple helper class than converts a raw exception to a JSON printable object
 *
 * @author James Alfei
 */
public class ExceptionJsonHelper {

	private ExceptionJsonHelper() {
		throw new IllegalStateException("Static helper class");
	}

	public static ExceptionJsonWrapper convertExceptionToJsonObject(Exception e) {
		return new ExceptionJsonWrapper(e.getMessage());
	}

	private static class ExceptionJsonWrapper {

		private String errorMessage;

		ExceptionJsonWrapper(String message) {
			this.errorMessage = message;
		}

		@SuppressWarnings("unused")
		public String getErrorMessage() {
			return errorMessage;
		}

        @Override
        public String toString() {
            return errorMessage;
        }
    }

}
