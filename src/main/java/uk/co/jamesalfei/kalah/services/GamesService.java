package uk.co.jamesalfei.kalah.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import uk.co.jamesalfei.kalah.beans.Game;
import uk.co.jamesalfei.kalah.beans.Pit;
import uk.co.jamesalfei.kalah.beans.Player;
import uk.co.jamesalfei.kalah.exceptions.GameNotFoundException;
import uk.co.jamesalfei.kalah.exceptions.GameNotYetStartedException;
import uk.co.jamesalfei.kalah.exceptions.GameOverException;
import uk.co.jamesalfei.kalah.exceptions.InGameException;
import uk.co.jamesalfei.kalah.exceptions.InvalidPitIdGivenException;
import uk.co.jamesalfei.kalah.exceptions.PitIsHomePitException;
import uk.co.jamesalfei.kalah.exceptions.PitNotOwnedByPlayerException;

/**
 * This class performs the main work of the application.
 *
 * It can start a new game and assign players to existing games, as well as
 * controlling whether the user input for a given move is valid. It also
 * determines whether a game is over
 *
 * @author James Alfei
 */
@ApplicationScoped
public class GamesService {

	private static final int NUMBER_OF_STONES = 6;
	private static final int NUMBER_OF_PLAYER_PITS = 6;
	private static final int NUMBER_OF_PLAYERS = 2;

	private List<Game> games = new ArrayList<>();

    /**
     * Starts a new game or adds a new player to an existing game if a game exists with only one player
     *
     * @param absolutePath The URI of the application used by the incoming request
     * @return The new (or existing) game for which the requester will be playing in
     */
	public Game startNewGame(URI absolutePath) {
		for (Game game : games) {
			if (game.getPlayers().size() < NUMBER_OF_PLAYERS) {
				game.getPlayers().add(new Player(game.getPlayers().size() + 1));
				return game;
			}
		}
		Game game = new Game(absolutePath.toString(), NUMBER_OF_PLAYERS, NUMBER_OF_PLAYER_PITS, NUMBER_OF_STONES);
		games.add(game);

		return game;
	}

    /**
     * Performs a move on a game with a given ID and pit ID
     *
     * @param gameId The ID of the game on which to make the move
     * @param pitId The pit ID on which you would like to move
     * @return The {@link Game} after the move has been made
     * @throws InGameException Thrown if the move is invalid or another issue occurs with the move or game
     */
	public Game makeMove(String gameId, int pitId) throws InGameException {
		for (Game game : games) {
			if (game.getId().equals(gameId)) {

				isGameActive(game);
				isPitIdWithinRange(pitId, game);
				isPitPlayable(pitId, game);
				isPitMoveValidForPlayer(pitId, game);

				game.performMove(pitId);

				if (isGameOver(game)) {
					throw new GameOverException(game.finaliseGameAndDetermineWinner());
				}
				return game;
			}
		}
		throw new GameNotFoundException();
	}

    /**
     * Checks whether there are 2 players in the game
     *
     * @param game The game to be checked
     * @throws GameNotYetStartedException Thrown if there are not 2 players in the game
     */
	private void isGameActive(Game game) throws GameNotYetStartedException {
		if (game.getPlayers().size() != NUMBER_OF_PLAYERS) {
			throw new GameNotYetStartedException();
		}
	}

    /**
     * Checks whether the given pit is owned by the player
     *
     * @param pitId The pit ID to check
     * @param game The game in which the pit lives
     * @throws PitNotOwnedByPlayerException Thrown if the given pit ID is not owned by the current player
     */
	private void isPitMoveValidForPlayer(int pitId, Game game) throws PitNotOwnedByPlayerException {
		if (game.getBoard().getPits().get(pitId).getOwningPlayerId() != game.getCurrentPlayerTurn()) {
			throw new PitNotOwnedByPlayerException();
		}
	}

    /**
     * Checks whether the pit is a "playable" pit i.e. it is not a "home" pit
     *
     * @param pitId The given pit ID
     * @param game The game to be checked
     * @throws PitIsHomePitException Thrown if the pit at the given ID is in fact a home pit
     */
	private void isPitPlayable(int pitId, Game game) throws PitIsHomePitException {
		if (game.getBoard().getPits().get(pitId).isHomePit()) {
			throw new PitIsHomePitException();
		}
	}

    /**
     * Checks whether the given Pit ID is within the valid range of available pits
     *
     * @param pitId The supplied Pit ID
     * @param game The game to check
     * @throws InvalidPitIdGivenException Thrown if the pit ID given is invalid
     */
	private void isPitIdWithinRange(int pitId, Game game) throws InvalidPitIdGivenException {
		final String message = String.format("%s%s", "Please select a pit ID between 1 and ",
				game.getBoard().getPits().size());
		if (pitId < 0) {
			throw new InvalidPitIdGivenException(String.format("%s. %s", "Given pit ID is less than 1", message));
		}
		if (pitId > game.getBoard().getPits().size() - 1) {
			throw new InvalidPitIdGivenException(
					String.format("%s. %s", "Given pit ID exceeds the number of pits in this game", message));
		}
	}

    /**
     * Checks whether the game has ended
     *
     * @param game The game to be checked
     * @return True if the game has ended, false if not
     */
	private boolean isGameOver(Game game) {
		int runningStoneCount = 0;
		for (Pit pit : game.getBoard().getPits()) {
			if (pit.isHomePit()) {
				// We have no stones in a players row of pits, game is over
				if (runningStoneCount == 0) {
					return true;
				}
				runningStoneCount = 0; // Reset the count for next player run through
				continue;
			}
			runningStoneCount += pit.getNumberOfStones();
		}
		return false;
	}
}
