package uk.co.jamesalfei.kalah.exceptions;

/**
 * Thrown when the player supplies an invalid Pit ID e.g. -1 or 400
 *
 * @author James Alfei
 */
public class InvalidPitIdGivenException extends InGameException {

	private static final long serialVersionUID = 8127779771316586844L;

	public InvalidPitIdGivenException(String s) {
		super(s);
	}
}
