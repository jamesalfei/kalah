package uk.co.jamesalfei.kalah.exceptions;

/**
 * Thrown when a player selected a pit with no stones.
 *
 * @author James Alfei
 */
public class NoStonesInSelectedPitException extends InGameException {

	private static final long serialVersionUID = -7123034768985935506L;

	public NoStonesInSelectedPitException() {
		super("There are no stones in the selected pit. Please select another pit.");
	}
}
