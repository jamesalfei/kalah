package uk.co.jamesalfei.kalah.exceptions;

/**
 * Thrown if a player attempts to make a move for a game with a given ID that
 * does not exist.
 *
 * @author James Alfei
 */
public class GameNotFoundException extends InGameException {

	private static final long serialVersionUID = 3884199463227171822L;

	public GameNotFoundException() {
		super("No game found for given game ID");
	}

}
