package uk.co.jamesalfei.kalah.exceptions;

import uk.co.jamesalfei.kalah.beans.HomePit;
import uk.co.jamesalfei.kalah.beans.Pit;

/**
 * This exception, when thrown, marks the end of the game. The winning
 * {@link HomePit} will be attached to this exception.
 * 
 * @author James Alfei
 */
public class GameOverException extends InGameException {

	private static final long serialVersionUID = -2615078418592081788L;

	private Pit winningPit;

	public GameOverException(Pit winningPit) {
		super("Game over!");
		this.winningPit = winningPit;
	}

	public Pit getWinningPit() {
		return winningPit;
	}
}
