package uk.co.jamesalfei.kalah.exceptions;

/**
 * Thrown when a player selects a Pit ID which is a home pit (and thus
 * unplayable)
 *
 * @author James Alfei
 */
public class PitIsHomePitException extends InGameException {

	private static final long serialVersionUID = 1505430617251552882L;

	public PitIsHomePitException() {
		super("Given pit ID is a home pit. Please select another pit ID.");
	}
}
