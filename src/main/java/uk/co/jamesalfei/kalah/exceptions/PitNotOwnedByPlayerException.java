package uk.co.jamesalfei.kalah.exceptions;

/**
 * Thrown when a player selects a Pit ID that is now owned by them (and thus
 * unplayable)
 *
 * @author James Alfei
 *
 */
public class PitNotOwnedByPlayerException extends InGameException {

	private static final long serialVersionUID = -436607528592239343L;

	public PitNotOwnedByPlayerException() {
		super("The selected Pit ID is not owned by you. Please select another pit ID.");
	}
}
