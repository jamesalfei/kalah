package uk.co.jamesalfei.kalah.exceptions;

public abstract class InGameException extends Exception {

	private static final long serialVersionUID = -2521797161472871619L;

	public InGameException(String message) {
		super(message);
	}
}
