package uk.co.jamesalfei.kalah.exceptions;

import uk.co.jamesalfei.kalah.beans.HomePit;

/**
 * Thrown when a {@link HomePit} for the current player cannot be found
 *
 * @author James Alfei
 */
public class NoPlayerHomeFound extends InGameException {

	private static final long serialVersionUID = 5479932830978065983L;

	public NoPlayerHomeFound(String s) {
		super(s);
	}
}
