package uk.co.jamesalfei.kalah.exceptions;

/**
 * Thrown if a player attempts to make a move before there are enough players in
 * the game.
 *
 * @author James Alfei
 */
public class GameNotYetStartedException extends InGameException {

	private static final long serialVersionUID = 881994229104343354L;

	public GameNotYetStartedException() {
		super("There are not yet enough players in the game. Please wait for another player to join");
	}
}
