package uk.co.jamesalfei.kalah.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class Pit implements IPit{

    private int numberOfStones;

    private int owningPlayerId;

    @JsonIgnore
    private boolean isHomePit;

    public Pit(int numberOfStones, int owningPlayerId, boolean isHomePit) {
        this.numberOfStones = numberOfStones;
        this.owningPlayerId = owningPlayerId;
        this.isHomePit = isHomePit;
    }

    public int getNumberOfStones() {
        return numberOfStones;
    }

    public void setNumberOfStones(int numberOfStones) {
        this.numberOfStones = numberOfStones;
    }

    public int getOwningPlayerId() {
        return owningPlayerId;
    }

    public boolean isHomePit() {
        return isHomePit;
    }
}
