package uk.co.jamesalfei.kalah.beans;

import java.util.UUID;

public class Player {

    private String id;

    private int playerNumber;

    public Player(int playerNumber) {
        this.id = UUID.randomUUID().toString();
        this.playerNumber = playerNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }
}
