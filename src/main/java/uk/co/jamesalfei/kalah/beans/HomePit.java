package uk.co.jamesalfei.kalah.beans;

/**
 * A special kind of pit that cannot have all stones taken from it.
 *
 * @author James Alfei
 *
 */
public class HomePit extends Pit {

	public HomePit(int owningPlayerId) {
		super(0, owningPlayerId, true);
	}

	@Override
	public int takeAllFromPit() {
		return 0;
	}
}
