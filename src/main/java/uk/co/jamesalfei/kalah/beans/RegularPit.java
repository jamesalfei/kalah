package uk.co.jamesalfei.kalah.beans;

public class RegularPit extends Pit {

	public RegularPit(int numberOfStones, int owningPlayerId) {
		super(numberOfStones, owningPlayerId, false);
	}

	@Override
	public int takeAllFromPit() {
		int stonesInPit = getNumberOfStones();
		setNumberOfStones(0);
		return stonesInPit;
	}
}
