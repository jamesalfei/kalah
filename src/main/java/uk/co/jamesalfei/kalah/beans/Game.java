package uk.co.jamesalfei.kalah.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import uk.co.jamesalfei.kalah.exceptions.NoPlayerHomeFound;
import uk.co.jamesalfei.kalah.exceptions.NoStonesInSelectedPitException;

/**
 * The main bean of the Kalah game. This class stores all current moves, players
 * etc. for any running game of Kalah
 *
 * @author James Alfei
 */
public class Game {

	@NotNull
	private String id;

	@NotNull
	private String uri;

	@NotNull
	private Board board;

	@NotNull
	private List<Player> players;

	private int currentPlayerTurn = 1;

	public Game(String gamePath, int numberOfPlayers, int pitCount, int stoneCount) {
		this.id = UUID.randomUUID().toString();
		this.uri = gamePath + "/" + id;
		this.board = new Board(numberOfPlayers, pitCount, stoneCount);
		this.players = new ArrayList<>();
		this.players.add(new Player(1)); // As this is a brand new game, we'll always start from player 1
	}

	public String getId() {
		return id;
	}

	public String getUri() {
		return uri;
	}

	public Board getBoard() {
		return board;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public int getCurrentPlayerTurn() {
		return currentPlayerTurn;
	}

	public GameOverview getGameOverview() {
		return new GameOverview();
	}

	public GameStatus getGameStatus() {
		return new GameStatus();
	}

	/**
	 * Performs the move for the player for the given PitID.
	 *
	 * @param pitId The valid {@link Pit} ID
	 * @throws NoPlayerHomeFound              If we somehow cannot find the current
	 *                                        players {@link HomePit}, this
	 *                                        exception will be thrown.
	 * @throws NoStonesInSelectedPitException If the selected {@link Pit} has no
	 *                                        stones in it, this exception will be
	 *                                        thrown.
	 */
	public void performMove(int pitId) throws NoPlayerHomeFound, NoStonesInSelectedPitException {
		Pit startingPit = getBoard().getPits().get(pitId);

		if (startingPit.getNumberOfStones() == 0) {
			throw new NoStonesInSelectedPitException();
		}

		int numberOfStonesToSow = startingPit.takeAllFromPit();
		int finalPitId = sowSeedsToPits(pitId, numberOfStonesToSow);

		// Get the final Pit on which we will land.
		Pit landingPit = getBoard().getPits().get(finalPitId);

		// If the last stone was in your Home pit, you get another turn
		if (pitIsCurrentPlayerHome(landingPit)) {
			return; // Do nothing else. Don't increment player counter
		}

		stealOpponentsStonesIfPossible(finalPitId, landingPit);
		incrementCurrentPlayerCounter();
	}

	/**
	 * If the game is over, we must finalise the game and determine who has the most
	 * stones. We collect all stones from each players {@link RegularPit}s and move
	 * them to the players {@link HomePit}. After doing this, whoever has the most
	 * stones in the home pit wins.
	 *
	 * @return The winning {@link HomePit}
	 */
	public Pit finaliseGameAndDetermineWinner() {
		int runningStoneCount = 0;

		// Gather all stones from player pits and add them to home pit
		for (Pit pit : getBoard().getPits()) {
			if (pit.isHomePit()) {
				pit.setNumberOfStones(pit.getNumberOfStones() + runningStoneCount);
				runningStoneCount = 0;
			}
			runningStoneCount += pit.takeAllFromPit();
		}

		// Determine the winning player from home stones
		Pit winningPit = null;
		for (Pit pit : getBoard().getPits()) {
			if (pit.isHomePit() && (winningPit == null || winningPit.getNumberOfStones() < pit.getNumberOfStones())) {
				winningPit = pit;
			}
		}

		return winningPit;
	}

	/**
	 * If the players final stone lands on one of their own pits AND it is now the
	 * only stone in that pit, you can steal the opponents stones in the pit
	 * directly opposite to yours (if they have stones).
	 *
	 * Note: As Kalah is played in an anti-clockwise fashion, the direct opposite
	 * pit must be calculated as though your list of pits is to be accessed
	 * cyclically.
	 *
	 * @param finalPitId The ID of the pit in which the final stone landed
	 * @param landingPit The actual pit in which the final stone landed
	 * @throws NoPlayerHomeFound If for some reason we cannot find the current
	 *                           players home pit, this exception will be thrown
	 */
	private void stealOpponentsStonesIfPossible(int finalPitId, Pit landingPit) throws NoPlayerHomeFound {
		if ((landingPit.getNumberOfStones() == 1) && (landingPit.getOwningPlayerId() == currentPlayerTurn)) {

			int oppositeId = 0;
			for (int index = 0; index < getBoard().getPlayablePits().size(); index++) {
				if (getBoard().getPlayablePits().get(index) == landingPit) {
					oppositeId = index;
					break;
				}
			}

			Pit opposingPit = getBoard().getPlayablePits().get(getBoard().getPlayablePits().size() - 1 - oppositeId);
			if (opposingPit.getNumberOfStones() > 0) {
				Pit playerHome = getCurrentPlayerHome();
				playerHome.setNumberOfStones(playerHome.getNumberOfStones() + landingPit.takeAllFromPit());
				playerHome.setNumberOfStones(playerHome.getNumberOfStones() + opposingPit.takeAllFromPit());
			}
		}
	}

	/**
	 * Performs the sowing of a all seeds in a pit to the other pits anti-clockwise.
	 * Each sow deposits one seed into a pit.
	 *
	 * @param pitId               The ID of the pit where the sowing must start
	 * @param numberOfStonesToSow The original number of stones in the pit selected
	 *                            by the user
	 * @return The ID of the final pit which stones will be sowed to
	 */
	private int sowSeedsToPits(int pitId, int numberOfStonesToSow) {
		int homePitOffset = 0;
		int totalNumberOfPits = getBoard().getPits().size();

		// I was really tempted to call this "piterator"...
		for (int pitIterator = 1; pitIterator <= numberOfStonesToSow + homePitOffset; pitIterator++) {
			Pit pitInPlay = getBoard().getPits().get((pitIterator + pitId) % totalNumberOfPits);

			// Perform a check to ensure we don't sow a stone to the opponent players house!
			if (pitIsOpposingPlayerHome(pitInPlay)) {
				homePitOffset++;
				continue;
			}

			pitInPlay.setNumberOfStones(pitInPlay.getNumberOfStones() + 1);
		}

		return (numberOfStonesToSow + homePitOffset + pitId) % totalNumberOfPits;
	}

	/**
	 * Gets the current player home/
	 *
	 * @return The current player home
	 * @throws NoPlayerHomeFound If no home can be found for the current player,
	 *                           this exception is thrown.
	 */
	private Pit getCurrentPlayerHome() throws NoPlayerHomeFound {
		for (Pit pit : getBoard().getPits()) {
			if (pitIsCurrentPlayerHome(pit)) {
				return pit;
			}
		}
		throw new NoPlayerHomeFound(
				"No home for current player could be found. Please contact the application creator!");
	}

	private boolean pitIsCurrentPlayerHome(Pit pit) {
		return pit.isHomePit() && (pit.getOwningPlayerId() == getCurrentPlayerTurn());
	}

	private boolean pitIsOpposingPlayerHome(Pit pit) {
		return pit.isHomePit() && (pit.getOwningPlayerId() != getCurrentPlayerTurn());
	}

	private void incrementCurrentPlayerCounter() {
		currentPlayerTurn++;
		if (currentPlayerTurn > getPlayers().size()) {
			currentPlayerTurn = 1;
		}
	}

	/**
	 * Private class to create an object for JSON representation ONLY
	 */
	@SuppressWarnings("unused")
	private class GameOverview {

		private String id;

		private String uri;

		GameOverview() {
			this.id = Game.this.getId();
			this.uri = Game.this.getUri();
		}

		public String getId() {
			return id;
		}

		public String getUri() {
			return uri;
		}
	}

	/**
	 * Private class to create an object for JSON representation ONLY
	 */
	@SuppressWarnings("unused")
	private class GameStatus extends GameOverview {

		private Map<Integer, Integer> status;

		GameStatus() {
			super();

			status = new HashMap<>();
			List<Pit> currentPits = Game.this.getBoard().getPits();

			for (int pitIndex = 0; pitIndex < currentPits.size(); pitIndex++) {
				status.put(pitIndex + 1, currentPits.get(pitIndex).getNumberOfStones());
			}
		}

		public Map<Integer, Integer> getStatus() {
			return status;
		}

		@Override
		public String toString() {
			return status.toString();
		}
	}
}
