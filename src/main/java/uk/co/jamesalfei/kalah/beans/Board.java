package uk.co.jamesalfei.kalah.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * This class acts as a logical holder for all the pits on the board.
 *
 * @author James Alfei
 */
public class Board {

	private List<Pit> pits;

	Board(int numberOfPlayers, int pitCount, int stoneCount) {
		this.pits = new ArrayList<>();

		for (int playerId = 1; playerId <= numberOfPlayers; playerId++) {
			for (int pitId = 0; pitId < pitCount; pitId++) {
				this.pits.add(new RegularPit(stoneCount, playerId));
			}
			this.pits.add(new HomePit(playerId));
		}
	}

	public List<Pit> getPits() {
		return pits;
	}

	/**
	 * Returns only the {@link RegularPit}s from the board
	 *
	 * @return A list of {@link RegularPit}s
	 */
	public List<Pit> getPlayablePits() {
		List<Pit> playablePits = new ArrayList<>();

		for (Pit pit : pits) {
			if (!pit.isHomePit()) {
				playablePits.add(pit);
			}
		}

		return playablePits;
	}

}
