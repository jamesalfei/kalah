package uk.co.jamesalfei.kalah;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.jboss.logging.Logger;

/**
 * The main Kalah application. Starts up the application and automatically scans
 * for classes to be added
 *
 * @author James Alfei
 */
@ApplicationPath("Kalah")
public class KalahApplication extends Application {

	private static final Logger LOGGER = Logger.getLogger(GamesEndpoint.class);

	public KalahApplication() {
		LOGGER.info("Application starting up");
	}
}
