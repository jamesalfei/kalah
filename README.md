# Kalah

This web app allows a simple game of 2-player, 6-stone Kalah to be played on the same machine via a REST API.

## Usage

To run the application, simply build the application using `mvn clean install` and deploy the `Kalah.war` file in the target directory to your favourite application server.

To access the game, please follow the Open API documentation of the service which can be found (after deployment) at GET [Localhost OpenAPI](http://localhost:8081/Kalah/openapi.json)

Please note: This application was developed and tested using Wildfly 14 application server.

## Documentation

The application is fully documented using Javadoc comments. Additionally, Swagger API documentation is also available and can be found at the link in the usage section.

## Architecture

This application was designed to be simple, without using any unnecessary frameworks and tools. Instead, a focus on the code structure, formatting, test and documentation have taken place.

The application uses a single central endpoint class which then moves the heavy lifting onto the main GameService. Games are stored in memory for the duration of the lifetime of the JVM. No persistence layer or technologies were used in this example. No UI is provided for this application as this is a pure standalone API based implementation of the game.

## Future improvements

* Introduce persistence layer for storing of game progress
* Web UI for visual representation of the active game
* More detailed JSON output for the web layer (e.g. Current player ID, next player ID etc.)
* Arquillian based tests to ensure that the Web endpoint layer works as expected on a number of different container application servers
* Swagger UI for web documentation as part of the deployed API (always up to date)